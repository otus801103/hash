#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Структура одного элемента хеш-таблицы */
typedef struct HashItem {
    char* key;           // Ключ элемента (строка)
    int value;           // Значение, связанное с ключом (целое число)
    bool is_occupied;    // Флаг заполненности элемента
} HashItem;

/* Структура хеш-таблицы */
typedef struct HashTable {
    HashItem* items;      // Массив элементов таблицы
    size_t capacity;      // Общее количество слотов в хеш-таблице
    size_t size;          // Количество занятых слотов в хеш-таблице
    float load_factor;    // Фактор загрузки (отношение количества элементов к емкости)
} HashTable;

/* Объявление функции, реализующей strcpy, которой нет в C11 */
char* my_strdup(const char* str) {
    size_t len = strlen(str) + 1;
    char* dup = malloc(len);
    if (dup) {
        memcpy(dup, str, len);
    }
    return dup;
}

/* Хеш-функция */
unsigned long hash(const char* str, size_t size) {
    unsigned long hash = 5381;     // Начальное значение хеша
    int c;
    while ((c = *str++) != 0) {
        hash = ((hash << 5) + hash) + (unsigned char)c; // (hash * 33) ^ c
    }
    return hash % size;            // Используем остаток от деления для индексации массива
}

/* Функция создания хеш-таблицы с начальной емкостью */
HashTable* create_hash_table(size_t initial_capacity) {
    HashTable* table = malloc(sizeof(HashTable)); // Выделение памяти для структуры таблицы
    if (!table) return NULL;

    table->capacity = initial_capacity;
    table->size = 0;
    table->load_factor = 0.75f; // Идеальный фактор загрузки для хеш-таблицы
    table->items = calloc(table->capacity, sizeof(HashItem));
    if (!table->items) {
        free(table);
        return NULL;
    }
    return table;
}

/* Функция освобождения памяти хеш-таблицы */
void free_hash_table(HashTable* table) {
    if (!table) return;
    for (size_t i = 0; i < table->capacity; i++) {
        if (table->items[i].is_occupied) {
            free(table->items[i].key);
        }
    }
    free(table->items);
    free(table);
}

/* Объявление прототипов функций, определенных позже */
bool hash_table_insert(HashTable* table, const char* key, int value);
bool resize_hash_table(HashTable* table, size_t new_capacity);

/* Функция вставки ключа и значения в хеш-таблицу с возможным расширением */
bool insert(HashTable* table, const char* key, int value) {
    if (!table) return false;

    // Проверка на необходимость расширения таблицы
    if ((float)table->size / table->capacity >= table->load_factor) {
        if (!resize_hash_table(table, table->capacity * 2)) {
            return false;
        }
    }

    return hash_table_insert(table, key, value);
}

/* Функция вставки ключа и значения в хеш-таблицу без расширения */
bool hash_table_insert(HashTable* table, const char* key, int value) {
    unsigned long idx = hash(key, table->capacity); // Получение хеш-индекса
    HashItem* item = table->items + idx;            // Получение указателя на элемент
    size_t step = 0;

    // Перебор слотов при возникновении коллизии (линейное пробирование)
    while (item->is_occupied) {
        if (strcmp(item->key, key) == 0) {
            item->value += value;
            return true;
        }
        idx = (idx + 1) % table->capacity; // Поиск следующего слота
        item = table->items + idx;
        step++;
        if (step >= table->capacity) return false; // Таблица переполнена
    }

    item->key = my_strdup(key);
    if (!item->key) return false;

    item->value = value;
    item->is_occupied = true;
    table->size++;
    return true;
}

/* Функция поиска значения по ключу */
int search(HashTable* table, const char* key) {
    unsigned long idx = hash(key, table->capacity); // Получение хеш-индекса
    HashItem* item = table->items + idx;            // Получение указателя на элемент
    size_t step = 0;

    // Поиск элемента с указанным ключом с учетом коллизий
    while (item->is_occupied) {
        if (strcmp(item->key, key) == 0) {
            return item->value;
        }
        idx = (idx + 1) % table->capacity;
        item = table->items + idx;
        step++;
        if (step >= table->capacity) return -1;
    }
    return -1;
}

/* Функция расширения хеш-таблицы до новой емкости */
bool resize_hash_table(HashTable* table, size_t new_capacity) {
    HashTable* new_table = create_hash_table(new_capacity); // Создаем новую таблицу с увеличенной емкостью
    if (!new_table) return false;

    // Перенос элементов из старой таблицы в новую
    for (size_t i = 0; i < table->capacity; i++) {
        HashItem old_item = table->items[i];
        if (old_item.is_occupied) {
            hash_table_insert(new_table, old_item.key, old_item.value); // Вставка в новую таблицу
            // Не освобождаем ключ, так как он теперь управляется новой таблицей
        }
    }

    free(table->items);
    table->items = new_table->items;
    table->capacity = new_capacity;
    free(new_table);
    return true;
}

/* Функция подсчета частоты слов в файле */
void count_word_frequencies(HashTable* table, const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        return;
    }

    char word_buffer[256];
    while (fscanf(file, "%255s", word_buffer) != EOF) {
        insert(table, word_buffer, 1);
    }

    fclose(file);
}

/* Функция печати частот слов из хеш-таблицы */
void print_frequencies(HashTable* table) {
    printf("Word frequencies:\n");
    for (size_t i = 0; i < table->capacity; i++) {
        if (table->items[i].is_occupied) {
            printf("%s: %d\n", table->items[i].key, table->items[i].value);
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char* filename = argv[1];
    HashTable* hashtable = create_hash_table(1024);

    count_word_frequencies(hashtable, filename);
    print_frequencies(hashtable);

    free_hash_table(hashtable);
    return EXIT_SUCCESS;
}

